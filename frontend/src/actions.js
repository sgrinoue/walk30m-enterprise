const prefix = '@@walk30m/';

export const CALCULATION_START = prefix + 'CALCULATION_START';
export const CALCULATION_PROGRESS = prefix + 'CALCULATION_PROGRESS';
export const CALCULATION_COMPLETE = prefix + 'CALCULATION_COMPLETE';
export const CALCULATION_SYNC = prefix + 'CALCULATION_SYNC';
export const CALCULATION_SYNC_SUCCESS = prefix + 'CALCULATION_SYNC_SUCCESS';
export const CALCULATION_ERROR = prefix + 'CALCULATION_ERROR';
export const MAP_MARKER_MOVED = prefix + 'MAP_MARKER_MOVED';
export const REQUEST_DOWNLOAD = prefix + 'REQUEST_DOWNLOAD';
export const ADDRESS_CHANGED = prefix + 'ADDRESS_CHANGED';
export const ADDRESS_CHOOSED = prefix + 'ADDRESS_CHOOSED';
export const INVERSE_GEOCODE_DONE = prefix + 'INVERSE_GEOCODE_DONE';
