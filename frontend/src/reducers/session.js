import {FETCH_END, USER_LOGIN_SUCCESS, AUTH_LOGOUT} from 'react-admin';
import jwt from 'jsonwebtoken';

const getAccountInformation = () => {
  const token = localStorage.getItem('token');

  if (!token) return {};

  const {aud: tenantId, sub: id, displayName, lastSignIn, role} = jwt.decode(
    token,
  );

  return {
    tenantId,
    id,
    displayName,
    lastSignIn,
    role,
  };
};

const initialState = {
  account: getAccountInformation(),
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_END:
      return {
        ...state,
        account: getAccountInformation(),
      };
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        account: getAccountInformation(),
      };
    case AUTH_LOGOUT:
      return {
        ...state,
        account: {},
      };
    default:
      return state;
  }
};
