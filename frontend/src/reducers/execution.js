import {
  CALCULATION_START,
  CALCULATION_PROGRESS,
  CALCULATION_SYNC_SUCCESS,
} from '../actions';

const initialState = {
  id: null,
  progress: null,
  vertices: [],
};

export default (state = initialState, action) => {
  const {type, payload} = action;

  switch (type) {
    case CALCULATION_START:
      return {
        ...state,
        id: payload,
        progress: 0,
        vertices: [],
      };
    case CALCULATION_PROGRESS:
      return {
        ...state,
        progress: payload.value,
        vertices: payload.vertices,
      };
    case CALCULATION_SYNC_SUCCESS:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
