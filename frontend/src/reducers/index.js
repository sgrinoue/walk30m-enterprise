import executionReducer from './execution';
import explorerReducer from './explorer';
import sessionReducer from './session';

export const execution = executionReducer;

export const session = sessionReducer;

export const explorer = explorerReducer;
