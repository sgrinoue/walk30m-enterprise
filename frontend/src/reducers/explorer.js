import {LOCATION_CHANGE} from 'react-router-redux';
import {
  INVERSE_GEOCODE_DONE,
  ADDRESS_CHOOSED,
  ADDRESS_CHANGED,
  MAP_MARKER_MOVED,
} from '../actions';

const getSavedConditions = () =>
  JSON.parse(localStorage.getItem('saved_conditions') || null) || {};
const initialState = {
  addressSuggestions: [],
  desiredMapBounds: undefined,
  initialValues: {
    mode: 'WALKING',
    preference: 'BALANCE',
    time: 30,
    originCoordinate: {lat: 35.9, lng: 139.7},
    originAddress: null,
    avoidFerries: false,
    avoidTolls: false,
    avoidHighways: false,
    ...getSavedConditions(),
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOCATION_CHANGE:
      if (action.payload.pathname !== '/executions/create') return state;

      return {
        ...initialState,
        initialValues: {
          ...initialState.initialValues,
          ...getSavedConditions(),
        },
      };
    case MAP_MARKER_MOVED:
      return {
        ...state,
        addressSuggestions: [],
      };
    case ADDRESS_CHANGED:
      return {
        ...state,
        addressSuggestions:
          action.payload === '' ? [] : state.addressSuggestions,
      };
    case ADDRESS_CHOOSED:
      return {
        ...state,
        addressSuggestions: [],
        desiredMapBounds: action.payload.geometry.bounds,
      };
    case INVERSE_GEOCODE_DONE:
      return {
        ...state,
        addressSuggestions: action.payload,
      };
    default:
      return state;
  }
};
