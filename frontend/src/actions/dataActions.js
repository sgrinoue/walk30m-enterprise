import {CRUD_CREATE, CREATE} from 'react-admin';

export const crudCreate = (
  resource,
  data,
  basePath,
  redirectTo,
  onSuccessMessage,
) => ({
  type: CRUD_CREATE,
  payload: {
    data,
  },
  meta: {
    resource,
    fetch: CREATE,
    onSuccess: {
      notification: {
        body: onSuccessMessage,
        level: 'info',
        messageArgs: {
          smart_count: 1,
        },
      },
      redirectTo,
      basePath,
    },
    onFailure: {
      notification: {
        body: 'ra.notification.http_error',
        level: 'warning',
      },
    },
  },
});
