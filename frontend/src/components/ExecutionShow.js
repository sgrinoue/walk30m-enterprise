import React from 'react';
import {connect} from 'react-redux';
import CardActions from '@material-ui/core/CardActions';
import GetAppIcon from '@material-ui/icons/GetApp';
import CancelIcon from '@material-ui/icons/Cancel';
import {Marker, Polygon} from 'react-google-maps';
import MaterialButton from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import {
  Show,
  SimpleShowLayout,
  TextField,
  ReferenceField,
  SelectField,
  DateField,
  FunctionField,
  Responsive,
} from 'react-admin';
import GoogleMap from './GoogleMap';
import Title from './Title';
import {
  modes,
  statuses,
  getBounds,
  preferences,
  describeExecution,
} from '../utils';
import {CALCULATION_ERROR, REQUEST_DOWNLOAD} from '../actions';
import styled from './ExecutionStyles';

const ExecutionTitle = ({record = {}}) => <Title label={`#${record.id}`} />;

const ShowHeader = ({className, record = {}}) => (
  <CardHeader
    className={className}
    subheader={`ID: ${record.id}`}
    title={describeExecution(record)}
  />
);

const getExecutedMapProps = record => {
  return {
    center: record.originCoordinate,
    zoom: 10,
    desiredBounds: record.resultPath ? getBounds(record.resultPath) : undefined,
    children: (
      <>
        {record.resultPath ? <Polygon path={record.resultPath} /> : null}
        <Marker position={record.originCoordinate} />
      </>
    ),
  };
};

const getExecutingMapProps = (record, {vertices = []} = {}) => {
  return {
    center: record.originCoordinate,
    zoom: 10,
    desiredBounds:
      vertices.length > 0
        ? getBounds(vertices.map(vertex => vertex.toJSON()))
        : undefined,
    children: (
      <>
        {vertices.length > 0 ? <Polygon path={vertices} /> : null}
        <Marker position={record.originCoordinate} />
      </>
    ),
  };
};

const Map = ({className, record = {}, execution = {}}) => {
  const isRunning = record.id === execution.id;
  const mapProps = isRunning
    ? getExecutingMapProps(record, execution)
    : getExecutedMapProps(record);

  return (
    <Card className={className}>
      {execution.progress !== null ? (
        <LinearProgress variant="determinate" value={execution.progress} />
      ) : null}
      <GoogleMap
        containerStyle={{
          height: '600px',
          maxHeight: 'calc(100vh - 120px)',
        }}
        {...mapProps}
      />
    </Card>
  );
};

const Actions = ({dispatch, record, execution, classes}) => (
  <CardActions>
    {record.status === 'completed' ? (
      <MaterialButton
        color="primary"
        onClick={() =>
          dispatch({
            type: REQUEST_DOWNLOAD,
            payload: {id: record.id, format: 'kml'},
          })
        }
      >
        <GetAppIcon />
        <span>KML 形式でエクスポート</span>
      </MaterialButton>
    ) : null}
    {record.status === 'completed' ? (
      <MaterialButton
        color="primary"
        onClick={() =>
          dispatch({
            type: REQUEST_DOWNLOAD,
            payload: {id: record.id, format: 'geojson'},
          })
        }
      >
        <GetAppIcon />
        <span>GeoJSON 形式でエクスポート</span>
      </MaterialButton>
    ) : null}
    {record.status === 'started' && execution.id === record.id ? (
      <MaterialButton
        className={classes.relaxedButton}
        variant="contained"
        color="secondary"
        onClick={() => dispatch({type: CALCULATION_ERROR})}
      >
        <CancelIcon />
        <span>中止</span>
      </MaterialButton>
    ) : null}
  </CardActions>
);

const LocationImage = ({data = {}}) => (
  <Responsive
    medium={null}
    large={
      <CardMedia
        style={{height: '150px'}}
        image={`https://maps.googleapis.com/maps/api/streetview?size=400x150&fov=120&location=${
          data.originAddress
        }&key=${process.env.REACT_APP_GOOGLE_API_KEY}`}
      />
    }
  />
);

const ExecutionShow = ({classes, execution, ...props}) => (
  <Show
    title={<ExecutionTitle />}
    className={[classes.container, classes.showContainer].join(' ')}
    aside={<Map className={classes.mapCard} execution={execution} />}
    actions={<LocationImage />}
    {...props}
  >
    <SimpleShowLayout>
      <ShowHeader className={classes.header} />
      <Actions classes={classes} execution={execution} {...props} />
      <FunctionField
        label="ステータス"
        source="status"
        render={({id, status}) =>
          execution.id === id && execution.progress !== null
            ? `実行中（${execution.progress}%）`
            : statuses[status]
        }
      />
      <ReferenceField
        label="実行ユーザー"
        source="userId"
        reference="accounts"
        linkType="show"
      >
        <TextField source="displayName" />
      </ReferenceField>
      <DateField label="実行日時" source="startDateTime" showTime />
      <TextField label="出発地点の住所" source="originAddress" />
      <SelectField
        choices={Object.entries(modes).map(([id, name]) => ({id, name}))}
        label="交通手段"
        source="mode"
      />
      <TextField label="移動時間（分）" source="time" />
      <SelectField
        label="計算時の優先事項"
        source="preference"
        choices={Object.entries(preferences).map(([id, name]) => ({id, name}))}
      />
      <FunctionField
        render={({avoidFerries}) => (avoidFerries ? 'いいえ' : 'はい')}
        label="フェリーを含むルートを許容する"
        source="avoidFerries"
      />
      <FunctionField
        render={({avoidTolls}) => (avoidTolls ? 'いいえ' : 'はい')}
        label="有料道路を含むルートを許容する"
        source="avoidTolls"
      />
      <FunctionField
        render={({avoidHighways}) => (avoidHighways ? 'いいえ' : 'はい')}
        label="高速道路を含むルートを許容する"
        source="avoidHighways"
      />
    </SimpleShowLayout>
  </Show>
);

export default styled(connect(({execution}) => ({execution}))(ExecutionShow));
