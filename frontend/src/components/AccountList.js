import React from 'react';
import {
  CardActions,
  List,
  ExportButton,
  TextField,
  SelectField,
  DateField,
} from 'react-admin';
import {Button} from 'ra-ui-materialui';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import {Link} from 'react-router-dom';
import Title from './Title';
import Datagrid from './DatagridWithProgress';
import {roles} from '../utils';

const AccountActions = ({
  bulkActions,
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
}) => (
  <CardActions>
    <Button component={Link} to="/invitations/create" label="ユーザーを招待">
      <PersonAddIcon />
    </Button>
    <ExportButton
      resource={resource}
      sort={currentSort}
      filter={filterValues}
      exporter={exporter}
    />
  </CardActions>
);

export default props => (
  <List
    title={<Title label="アカウント一覧" />}
    sort={{field: 'lastSignIn', order: 'DESC'}}
    actions={<AccountActions />}
    {...props}
  >
    <Datagrid>
      <TextField label="ログイン ID" source="id" />
      <TextField label="ユーザー名" source="displayName" />
      <SelectField
        label="ロール"
        source="role"
        choices={Object.entries(roles).map(([id, name]) => ({id, name}))}
      />
      <DateField label="最終ログイン日時" source="lastSignIn" showTime />
    </Datagrid>
  </List>
);
