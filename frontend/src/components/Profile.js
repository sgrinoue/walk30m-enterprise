import React from 'react';
import classnames from 'classnames';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core';

export default withStyles(() => ({
  container: {
    padding: '1.5em 1em',
    textAlign: 'center',
    backgroundColor: '#eee',
  },
  icon: {
    width: '2.4em',
    height: '2.4em',
  },
}))(({className, classes, account}) => (
  <div className={classnames(classes.container, className)}>
    <AccountCircle className={classes.icon} />
    <Typography>{account.tenantId}</Typography>
    <Typography>{account.displayName}</Typography>
  </div>
));
