import {withStyles} from '@material-ui/core/styles';

export default withStyles(({breakpoints}) => ({
  container: {
    flexDirection: 'column-reverse',
    [breakpoints.up('lg')]: {
      flexDirection: 'row',
    },
  },
  showContainer: {
    [breakpoints.up('lg')]: {
      '& > div > div:first-child': {
        padding: 0,
      },
    },
  },
  mapCard: {
    marginBottom: '1em',
    height: '600px',
    maxHeight: 'calc(100vh - 210px)',
    [breakpoints.up('lg')]: {
      marginBottom: 0,
      height: 'calc(100vh - 120px)',
      maxHeight: 'unset',
      minWidth: '50%',
      width: 'calc(60% - 1em)',
      marginLeft: '1em',
    },
  },
  header: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  relaxedControl: {
    width: 'unset',
    '& [role=radiogroup]': {
      width: 'unset',
      flexDirection: 'row',
    },
  },
  relaxedButton: {
    width: '100%',
    whiteSpace: 'pre',
  },
  button: {
    whiteSpace: 'pre',
  },
}));
