import React from 'react';
import {connect} from 'react-redux';
import SendIcon from '@material-ui/icons/Send';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import MaterialButton from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core';
import {
  RadioButtonGroupInput,
  ArrayInput,
  SimpleFormIterator,
  SimpleForm,
  TextInput,
  Create,
  Toolbar,
  email,
  required,
  minLength,
} from 'react-admin';
import {crudCreate} from '../actions/dataActions';
import Title from './Title';

const CreateToolbar = props => {
  const {
    basePath,
    dispatch,
    resource,
    formValues,
    saving,
    invalid,
    redirect,
  } = props;

  return (
    <Toolbar {...props}>
      <MaterialButton
        disabled={saving || invalid}
        variant="contained"
        color="primary"
        onClick={() =>
          dispatch(
            crudCreate(
              resource,
              formValues,
              basePath,
              redirect,
              'walk30m.notification.invitation_sent',
            ),
          )
        }
      >
        <SendIcon />
        <span>招待メールを送信</span>
      </MaterialButton>
    </Toolbar>
  );
};

const InvitationCreate = ({classes, ...props}) => (
  <Create title={<Title label="ユーザーの招待" />} {...props}>
    <SimpleForm
      toolbar={
        <CreateToolbar
          formValues={props.formValues}
          dispatch={props.dispatch}
        />
      }
      redirect={() => '/accounts'}
      defaultValue={{targets: [{email: '', role: 'editor'}]}}
    >
      <ArrayInput
        className={classes.form}
        source="targets"
        label="招待するユーザー"
        validate={minLength(1)}
      >
        <SimpleFormIterator>
          <TextInput
            label="メールアドレス"
            source="email"
            validate={[required(), email()]}
            type="email"
          />
          <RadioButtonGroupInput
            className={classes.roleInput}
            label="ロール"
            source="role"
            validate={required()}
            choices={[
              {
                id: 'administrator',
                name: (
                  <Tooltip title="アカウントの管理を含むすべての権限">
                    <Typography>管理者</Typography>
                  </Tooltip>
                ),
              },
              {
                id: 'editor',
                name: (
                  <Tooltip title="計算を実行できる標準の権限">
                    <Typography>編集者</Typography>
                  </Tooltip>
                ),
              },
              {
                id: 'viewer',
                name: (
                  <Tooltip title="計算結果の閲覧だけができる権限">
                    <Typography>閲覧者</Typography>
                  </Tooltip>
                ),
              },
            ]}
          />
        </SimpleFormIterator>
      </ArrayInput>
    </SimpleForm>
  </Create>
);

export default withStyles(() => ({
  form: {
    width: '100%',
  },
  roleInput: {
    width: '100%',
    '& > [role=radiogroup]': {
      flexDirection: 'row',
    },
  },
}))(
  connect(({form = {}}) => ({
    formValues: (form['record-form'] || {}).values || {},
  }))(InvitationCreate),
);
