import React from 'react';
import {Responsive} from 'react-admin';
import {withStyles} from '@material-ui/core';

const Title = ({classes, label}) => (
  <Responsive
    small={<span className={classes.title}>{label}</span>}
    medium={
      <div className={classes.title}>
        <span className={classes.appTitle}>walk30m for Business</span>
        <span>{label}</span>
      </div>
    }
  />
);

export default withStyles(() => ({
  title: {
    fontWeight: '200',
  },
  appTitle: {
    verticalAlign: 'bottom',
    fontSize: '0.65em',
    paddingRight: '8px',
    marginRight: '8px',
    opacity: 0.8,
    borderRight: '1px solid white',
  },
}))(Title);
