import React, {cloneElement} from 'react';
import {connect} from 'react-redux';
import classNames from 'classnames';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import {withStyles} from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import PersonIcon from '@material-ui/icons/Person';
import withWidth from '@material-ui/core/withWidth';
import {toggleSidebar as toggleSidebarAction} from 'ra-core';
import {MenuItemLink, Responsive, UserMenu, Headroom} from 'react-admin';
import compose from 'recompose/compose';
import Profile from './Profile';

const styles = theme => ({
  toolbar: {
    paddingRight: 24,
  },
  menuButton: {
    marginLeft: '0.5em',
    marginRight: '0.5em',
  },
  menuButtonIconClosed: {
    transition: theme.transitions.create(['transform'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    transform: 'rotate(0deg)',
  },
  menuButtonIconOpen: {
    transition: theme.transitions.create(['transform'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    transform: 'rotate(180deg)',
  },
  title: {
    flex: 1,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
  progressText: {
    margin: '0 .5em',
  },
  profile: {
    paddingLeft: '3.2em',
    paddingRight: '3.2em',
    backgroundColor: 'transparent',
  },
});

const AppBar = ({
  children,
  classes,
  className,
  logout,
  open,
  title,
  toggleSidebar,
  width,
  executionProgress,
  isLoggedIn,
  account,
  ...rest
}) => (
  <Headroom>
    <MuiAppBar
      className={className}
      color="secondary"
      position="static"
      {...rest}
    >
      <Toolbar
        disableGutters
        variant={width === 'xs' ? 'regular' : 'dense'}
        className={classes.toolbar}
      >
        <IconButton
          style={{visibility: isLoggedIn ? 'visible' : 'hidden'}}
          color="inherit"
          aria-label="open drawer"
          onClick={toggleSidebar}
          className={classNames(classes.menuButton)}
        >
          <MenuIcon
            classes={{
              root: open
                ? classes.menuButtonIconOpen
                : classes.menuButtonIconClosed,
            }}
          />
        </IconButton>
        <Typography
          variant="title"
          color="inherit"
          className={classes.title}
          id="react-admin-title"
        />
        {executionProgress !== null ? (
          <>
            <CircularProgress size={18} thickness={5} color="inherit" />
            <Typography className={classes.progressText} color="inherit">
              計算中(
              {executionProgress}
              %)...
            </Typography>
          </>
        ) : null}
        <Responsive
          small={null}
          medium={cloneElement(
            <UserMenu>
              <Profile account={account} className={classes.profile} />
              <MenuItemLink
                to={`/accounts/${account.id}/edit`}
                leftIcon={<PersonIcon />}
                primaryText="アカウント設定"
              />
            </UserMenu>,
            {logout},
          )}
        />
      </Toolbar>
    </MuiAppBar>
  </Headroom>
);

const enhance = compose(
  connect(
    state => ({
      locale: state.i18n.locale, // force redraw on locale change
      executionProgress: state.execution.progress,
      isLoggedIn: state.session.account.id !== undefined,
      account: state.session.account,
    }),
    {
      toggleSidebar: toggleSidebarAction,
    },
  ),
  withStyles(styles),
  withWidth(),
);

export default enhance(AppBar);
