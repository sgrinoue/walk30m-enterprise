import React from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import MaterialList from '@material-ui/core/List';
import MaterialButton from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import Card from '@material-ui/core/Card';
import SearchIcon from '@material-ui/icons/Search';
import {Marker} from 'react-google-maps';
import {
  SimpleForm,
  LongTextInput,
  NumberInput,
  Create,
  RadioButtonGroupInput,
  BooleanInput,
  Toolbar,
  required,
  minValue,
} from 'react-admin';
import GoogleMap from './GoogleMap';
import Title from './Title';
import styled from './ExecutionStyles';
import {modes, preferences} from '../utils';
import {ADDRESS_CHOOSED, ADDRESS_CHANGED, MAP_MARKER_MOVED} from '../actions';
import {crudCreate} from '../actions/dataActions';

const CreateToolbar = props => (
  <Toolbar {...props}>
    <MaterialButton
      disabled={props.saving || props.invalid}
      className={props.classes.relaxedButton}
      variant="contained"
      color="primary"
      onClick={() =>
        props.dispatch(
          crudCreate(
            props.resource,
            props.formValues,
            props.basePath,
            props.redirect,
            'walk30m.notification.execution_started',
          ),
        )
      }
    >
      <SearchIcon />
      <span>調べる</span>
    </MaterialButton>
  </Toolbar>
);

const Map = ({
  className,
  originCoordinate,
  onMarkerMove,
  markerPosition,
  desiredBounds,
}) =>
  originCoordinate ? (
    <Card className={className}>
      <GoogleMap
        desiredBounds={desiredBounds}
        defaultCenter={originCoordinate}
        defaultZoom={8}
      >
        <Marker
          draggable={true}
          position={markerPosition}
          onDragEnd={onMarkerMove}
        />
      </GoogleMap>
    </Card>
  ) : null;

class ExecutionCreate extends React.Component {
  handleAddressChange = (address = '') => {
    this.props.dispatch({type: ADDRESS_CHANGED, payload: address});
  };

  handleMarkerMove = ({latLng}) => {
    this.props.dispatch({type: MAP_MARKER_MOVED, payload: latLng.toJSON()});
  };

  renderAddressSuggestions() {
    const items = this.props.explorer.addressSuggestions;
    const use = result =>
      this.props.dispatch({type: ADDRESS_CHOOSED, payload: result});

    if (items.length === 0) return null;

    return (
      <MaterialList className={this.props.classes.relaxedControl}>
        {items.map((item, index) => (
          <ListItem key={index} style={{display: 'flex'}}>
            <Typography style={{flexGrow: 1}}>
              {item.formatted_address}
            </Typography>
            <Button color="primary" onClick={() => use(item)}>
              選択
            </Button>
          </ListItem>
        ))}
      </MaterialList>
    );
  }

  render() {
    const {explorer, account, classes, formValues, dispatch} = this.props;

    return (
      <Create
        title={<Title label="ホーム" />}
        className={classes.container}
        aside={
          <Map
            originCoordinate={formValues.originCoordinate}
            markerPosition={formValues.originCoordinate}
            desiredBounds={explorer.desiredMapBounds}
            className={classes.mapCard}
            onMarkerMove={this.handleMarkerMove}
          />
        }
        {...this.props}
      >
        <SimpleForm
          toolbar={
            <CreateToolbar
              classes={classes}
              formValues={formValues}
              dispatch={dispatch}
            />
          }
          defaultValue={{
            ...explorer.initialValues,
            userId: account.id,
            tenantId: account.tenantId,
          }}
        >
          <LongTextInput
            label="出発地点の住所"
            onChange={({target}) => this.handleAddressChange(target.value)}
            source="originAddress"
            validate={required()}
          />
          {this.renderAddressSuggestions()}
          <RadioButtonGroupInput
            label="交通手段"
            source="mode"
            className={classes.relaxedControl}
            choices={Object.entries(modes).map(([id, name]) => ({id, name}))}
          />
          <NumberInput
            label="移動時間（分）"
            source="time"
            validate={[required(), minValue(1)]}
          />
          <RadioButtonGroupInput
            label="計算時の優先項目"
            source="preference"
            className={classes.relaxedControl}
            choices={Object.entries(preferences).map(([id, name]) => ({
              id,
              name,
            }))}
          />
          <BooleanInput
            label="フェリーを含むルートを許容する"
            source="avoidFerries"
            className={classes.relaxedControl}
            format={v => !v}
            parse={v => !v}
          />
          <BooleanInput
            label="有料道路を含むルートを許容する"
            source="avoidTolls"
            className={classes.relaxedControl}
            format={v => !v}
            parse={v => !v}
          />
          <BooleanInput
            label="高速道路を含むルートを許容する"
            source="avoidHighways"
            className={classes.relaxedControl}
            format={v => !v}
            parse={v => !v}
          />
        </SimpleForm>
      </Create>
    );
  }
}

export default styled(
  connect(({explorer, session, execution, form = {}}) => ({
    explorer,
    account: session.account,
    execution,
    formValues: (form['record-form'] || {}).values || {},
  }))(ExecutionCreate),
);
