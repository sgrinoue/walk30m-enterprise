import React from 'react';
import {GoogleMap, withScriptjs, withGoogleMap} from 'react-google-maps';
import {compose} from 'recompose';
import {isEqual} from 'lodash';

const key = process.env.REACT_APP_GOOGLE_API_KEY;

class WrappedMap extends React.Component {
  constructor() {
    super();

    this.map = undefined;
    this.handleRef = this.handleRef.bind(this);
  }

  handleRef(el) {
    this.map = el;
  }

  componentDidMount() {
    const map = this.map;

    if (map && this.props.desiredBounds !== undefined) {
      map.fitBounds(this.props.desiredBounds);
    }
  }

  componentWillReceiveProps(nextProps) {
    const map = this.map;

    if (
      map &&
      nextProps.desiredBounds !== undefined &&
      !isEqual(this.props.desiredBounds, nextProps.desiredBounds)
    ) {
      map.fitBounds(nextProps.desiredBounds);
    }
  }

  render() {
    const {children, ...props} = this.props;

    return (
      <GoogleMap
        ref={this.handleRef}
        options={{gestureHandling: 'greedy', ...props.options}}
        {...props}>
        {children}
      </GoogleMap>
    );
  }
}

const ConfiguredMap = compose(
  BaseComponent => ({height, ...props}) => (
    <BaseComponent
      googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${key}`}
      loadingElement={<div style={{height: '100%'}} />}
      containerElement={<div style={{height: '100%'}} />}
      mapElement={<div style={{height: '100%'}} />}
      {...props}
    />
  ),
  withScriptjs,
  withGoogleMap,
)(WrappedMap);

export default ConfiguredMap;
