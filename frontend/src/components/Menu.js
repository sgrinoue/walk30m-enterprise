import React, {createElement} from 'react';
import {connect} from 'react-redux';
import {Responsive, MenuItemLink, WithPermissions} from 'react-admin';
import {withRouter} from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import HistoryIcon from '@material-ui/icons/History';
import PeopleIcon from '@material-ui/icons/People';
// import BookmarkIcon from '@material-ui/icons/Bookmark';
// import SettingsIcon from '@material-ui/icons/Settings';
// import SearchIcon from '@material-ui/icons/Search';
import Profile from './Profile';

const Menu = ({account, isSignedIn, onMenuClick, logout}) =>
  isSignedIn ? (
    <>
      <Responsive small={<Profile account={account} />} medium={null} />
      <WithPermissions
        render={({permissions}) =>
          permissions !== 'viewer' ? (
            <MenuItemLink
              to="/executions/create"
              primaryText="ホーム"
              leftIcon={createElement(HomeIcon)}
              onClick={onMenuClick}
            />
          ) : null
        }
      />
      <MenuItemLink
        isActive={(match, location) =>
          match && location.pathname !== '/executions/create'
        }
        to="/executions"
        primaryText="履歴"
        leftIcon={createElement(HistoryIcon)}
        onClick={onMenuClick}
      />
      {/*
          <MenuItemLink
            to="/accounts"
            primaryText="検索"
            leftIcon={createElement(SearchIcon)}
            onClick={onMenuClick}
          />
          <MenuItemLink
            to="/accounts"
            primaryText="ブックマーク"
            leftIcon={createElement(BookmarkIcon)}
            onClick={onMenuClick}
          />
          <MenuItemLink
            to="/accounts"
            primaryText="設定"
            leftIcon={createElement(SettingsIcon)}
            onClick={onMenuClick}
          />
          */}
      <WithPermissions
        render={({permissions}) =>
          ['system', 'administrator'].includes(permissions) ? (
            <MenuItemLink
              isActive={(match, location) =>
                match && location.pathname === '/accounts'
              }
              to="/accounts"
              primaryText="アカウント一覧"
              leftIcon={createElement(PeopleIcon)}
              onClick={onMenuClick}
            />
          ) : null
        }
      />
      <Responsive
        small={
          <MenuItemLink
            to={`/accounts/${account.id}/edit`}
            primaryText="アカウント設定"
            leftIcon={createElement(PersonIcon)}
            onClick={onMenuClick}
          />
        }
        medium={null}
      />
      <Responsive small={logout} medium={null} />
    </>
  ) : null;

export default withRouter(
  connect(({session}) => ({
    isSignedIn: !!session.account.id,
    account: session.account,
  }))(Menu),
);
