import {Datagrid} from 'react-admin';
import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const DatagridWithProgress = props => (
  <>
    <LinearProgress
      style={{visibility: props.isLoading ? 'visible' : 'hidden'}}
    />
    <Datagrid {...props}>{props.children}</Datagrid>
  </>
);

export default DatagridWithProgress;
