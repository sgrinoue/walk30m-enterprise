import React from 'react';
import {
  Edit,
  Toolbar,
  SaveButton,
  SimpleForm,
  TextField,
  SelectField,
  BooleanInput,
  FormDataConsumer,
  TextInput,
  required,
} from 'react-admin';
import Title from './Title';
import {roles} from '../utils';

const AccountEditToolbar = props => (
  <Toolbar {...props}>
    <SaveButton />
  </Toolbar>
);

const AccountEdit = props => (
  <Edit title={<Title label="アカウント設定" />} {...props}>
    <SimpleForm toolbar={<AccountEditToolbar />}>
      <TextField label="ログイン ID" source="id" />
      <TextInput
        label="ユーザー名"
        source="displayName"
        validate={required()}
      />
      <SelectField
        label="ロール"
        source="role"
        choices={Object.entries(roles).map(([id, name]) => ({id, name}))}
      />
      <BooleanInput label="パスワードを変更する" source="changePassword" />
      <FormDataConsumer>
        {({formData, ...rest}) =>
          formData.changePassword ? (
            <>
              <TextInput
                type="password"
                label="パスワード"
                source="password"
                validate={required()}
                {...rest}
              />
              <TextInput
                type="password"
                label="パスワード（確認）"
                source="passwordConfirm"
                validate={required()}
                {...rest}
              />
            </>
          ) : null
        }
      </FormDataConsumer>
    </SimpleForm>
  </Edit>
);

export default AccountEdit;
