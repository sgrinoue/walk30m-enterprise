import React from 'react';
import {connect} from 'react-redux';
import CheckIcon from '@material-ui/icons/Check';
import MaterialButton from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import jwt from 'jsonwebtoken';
import {
  DisabledInput,
  SimpleForm,
  TextInput,
  Create,
  Toolbar,
  required,
} from 'react-admin';
import Title from './Title';

const CreateToolbar = props => {
  const {saving, invalid, handleSubmitWithRedirect, redirect} = props;

  return (
    <Toolbar {...props}>
      <MaterialButton
        disabled={saving || invalid}
        variant="contained"
        color="primary"
        onClick={handleSubmitWithRedirect(redirect)}
      >
        <CheckIcon />
        <span>登録</span>
      </MaterialButton>
    </Toolbar>
  );
};

const AlreadySignedIn = () => (
  <CardContent>
    <Typography>
      続行するには, サインイン中のアカウントからログアウトしてください。
    </Typography>
  </CardContent>
);

const AccountCreate = ({isSignedIn, defaultValue, ...props}) => (
  <Create title={<Title label="アカウント登録" />} {...props}>
    {isSignedIn ? (
      <AlreadySignedIn />
    ) : (
      <SimpleForm
        toolbar={<CreateToolbar />}
        redirect={() => '/'}
        defaultValue={defaultValue}
      >
        <DisabledInput label="ログイン ID" source="id" />
        <TextInput label="名前" source="displayName" validate={required()} />
        <TextInput
          type="password"
          label="パスワード"
          source="password"
          validate={required()}
        />
        <TextInput
          type="password"
          label="パスワード（確認）"
          source="passwordConfirm"
          validate={required()}
        />
      </SimpleForm>
    )}
  </Create>
);

export default connect(({session, routing}) => {
  const [, token] =
    routing.location.search
      .split(/(&|\?)/g)
      .map(f => f.split('='))
      .find(([key]) => key === 'token') || [];
  const decoded = jwt.decode(token) || {};

  return {
    isSignedIn: session.account.id !== undefined,
    defaultValue: {
      id: decoded.sub,
      token,
    },
  };
})(AccountCreate);
