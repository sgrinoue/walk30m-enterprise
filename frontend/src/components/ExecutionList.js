import React from 'react';
import {connect} from 'react-redux';
import {
  List,
  ShowButton,
  TextField,
  DateField,
  SelectField,
  NumberField,
  Responsive,
  TextInput,
  SelectArrayInput,
  ReferenceArrayInput,
  Filter,
} from 'react-admin';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {describeExecution, modes, statuses} from '../utils';
import styled from './ExecutionStyles';
import Datagrid from './DatagridWithProgress';
import Title from './Title';

const Cards = ({ids, data, basePath, history}) => (
  <>
    {ids.map(id => (
      <Card
        key={id}
        style={{margin: '1em'}}
        onClick={() => history.push(`/executions/${id}/show`)}
      >
        <CardContent>
          <Typography style={{fontSize: '0.8em'}} color="textSecondary">
            {data[id].startDateTime.toLocaleString()}
          </Typography>
          <Typography style={{fontSize: '1.1em', margin: '0.5em 0'}}>
            {describeExecution(data[id])}
          </Typography>
          <Typography color="textSecondary">
            {`ステータス: ${statuses[data[id].status]}`}
          </Typography>
        </CardContent>
      </Card>
    ))}
  </>
);

const ExecutionFilter = props => (
  <Filter {...props}>
    <SelectArrayInput
      label="ステータス"
      source="status"
      style={{minWidth: '120px'}}
      choices={Object.entries(statuses).map(([id, name]) => ({id, name}))}
    />
    <TextInput label="出発地点の住所" source="originAddress" />
    <SelectArrayInput
      label="交通手段"
      source="mode"
      style={{minWidth: '120px'}}
      choices={Object.entries(modes).map(([id, name]) => ({id, name}))}
    />
    <ReferenceArrayInput
      label="実行ユーザー"
      source="userId"
      style={{minWidth: '120px'}}
      reference="accounts"
    >
      <SelectArrayInput optionText="displayName" />
    </ReferenceArrayInput>
  </Filter>
);

const ExecutionList = props => (
  <List
    title={<Title label="履歴" />}
    bulkActionButtons={false}
    sort={{field: 'startDateTime', order: 'DESC'}}
    filters={<ExecutionFilter />}
    {...props}
  >
    <Responsive
      small={<Cards history={props.history} />}
      medium={
        <Datagrid>
          <DateField label="実行日時" source="startDateTime" showTime />
          <SelectField
            choices={Object.entries(statuses).map(([id, name]) => ({id, name}))}
            label="ステータス"
            source="status"
          />
          <TextField label="出発地点の住所" source="originAddress" />
          <SelectField
            choices={Object.entries(modes).map(([id, name]) => ({id, name}))}
            label="交通手段"
            source="mode"
          />
          <NumberField label="移動時間（分）" source="time" />
          <ShowButton className={props.classes.button} />
        </Datagrid>
      }
    />
  </List>
);

export default connect()(styled(ExecutionList));
