import React from 'react';
import {Route, Redirect} from 'react-router';
import {Admin, Resource} from 'react-admin';
import createHistory from 'history/createHashHistory';
import {createMuiTheme} from '@material-ui/core/styles';
import ExecutionList from './components/ExecutionList';
import ExecutionShow from './components/ExecutionShow';
import ExecutionCreate from './components/ExecutionCreate';
import AccountList from './components/AccountList';
import AccountEdit from './components/AccountEdit';
import AccountCreate from './components/AccountCreate';
import InvitationCreate from './components/InvitationCreate';
import Layout from './components/Layout';
import dataProvider from './providers/data';
import authProvider from './providers/auth';
import * as extraReducers from './reducers';
import * as extraSagas from './sagas';
import messages from './i18n';

const theme = createMuiTheme({
  palette: {
    primary: {main: '#ff3d00'},
    secondary: {main: '#009688'},
    tonalOffset: 0.5,
  },
  typography: {
    fontFamily: 'sans-serif',
  },
});

const extraRoutes = [
  <Route exact path="/" render={() => <Redirect to="/executions/create" />} />,
];

const App = () => (
  <Admin
    locale="ja"
    theme={theme}
    authProvider={authProvider}
    dataProvider={dataProvider}
    customRoutes={extraRoutes}
    customReducers={extraReducers}
    customSagas={Object.values(extraSagas)}
    history={createHistory()}
    i18nProvider={locale => messages[locale]}
    appLayout={Layout}
  >
    {permissions => {
      const isPrivileged = ['system', 'administrator'].includes(permissions);

      return [
        <Resource
          name="executions"
          list={ExecutionList}
          show={ExecutionShow}
          create={permissions === 'viewer' ? null : ExecutionCreate}
        />,
        <Resource
          name="accounts"
          list={isPrivileged ? AccountList : null}
          edit={AccountEdit}
          create={AccountCreate}
        />,
        <Resource
          name="invitations"
          list={null}
          create={isPrivileged ? InvitationCreate : null}
        />,
      ];
    }}
  </Admin>
);

export default App;
