export const getBounds = coords =>
  coords.reduce(
    ({north, east, south, west}, {lat, lng}) => ({
      north: north === undefined ? lat : Math.max(north, lat),
      east: east === undefined ? lng : Math.max(east, lng),
      south: south === undefined ? lat : Math.min(south, lat),
      west: west === undefined ? lng : Math.min(west, lng),
    }),
    {},
  );

export const roles = {
  system: 'システムユーザー',
  administrator: '管理者',
  editor: '編集者',
  viewer: '閲覧者',
};

export const modes = {
  WALKING: '徒歩',
  DRIVING: '自動車',
};

export const statuses = {
  started: '開始',
  completed: '完了',
  aborted: '中止',
};

export const preferences = {
  SPEED: '計算の速さ',
  BALANCE: 'バランス（推奨）',
  PRECISION: '正確さ',
};

export const describeExecution = ({originAddress, mode, time}) => {
  return `${originAddress} から ${modes[mode]} で ${time} 分の範囲`;
};
