module.exports = {
  ra: {
    action: {
      add: '追加',
      delete: '削除',
      show: '詳細',
      list: '一覧',
      save: '保存',
      create: '作成',
      edit: '編集',
      cancel: 'キャンセル',
      refresh: '更新',
      add_filter: 'フィルターを追加',
      remove: '削除',
      remove_filter: 'フィルターを削除',
      export: 'エクスポート',
      bulk_actions: '%{smart_count} 件を選択中',
    },
    boolean: {
      true: 'はい',
      false: 'いいえ',
    },
    page: {
      list: '%{name} リスト',
      edit: '%{name} #%{id}',
      show: '%{name} #%{id}',
      create: '%{name} を作成',
      delete: '%{name} #%{id} を削除',
      dashboard: 'ダッシュボード',
    },
    input: {
      image: {
        upload_several:
          'アップロードするファイルをドロップ、または選択してください。',
        upload_single:
          'アップロードするファイルをドロップ、または選択してください。',
      },
    },
    message: {
      yes: 'はい',
      no: 'いいえ',
      are_you_sure: '本当によろしいですか?',
      about: '詳細',
    },
    navigation: {
      no_results: '該当するデータがありません。',
      page_out_of_boundaries: '無効なページ指定です。',
      page_out_from_end: '無効なページ指定です。',
      page_out_from_begin: '無効なページ指定です。',
      page_range_info: '%{total} 件中 %{offsetBegin}-%{offsetEnd}',
      page_rows_per_page: 'ページあたりの件数',
      next: '次へ',
      prev: '前へ',
    },
    auth: {
      username: 'ユーザー名',
      password: 'パスワード',
      sign_in: 'ログイン',
      sign_in_error: 'ログインに失敗しました。',
      logout: 'ログアウト',
    },
    notification: {
      updated: '更新されました。',
      created: '作成されました。',
      deleted: '削除されました。',
      item_doesnt_exist: '存在しないアイテムです。',
      http_error: 'サーバーエラー',
    },
    validation: {
      required: '必須',
      minLength: '%{min} 文字以上で入力してください。',
      maxLength: '%{max} 文字以内で入力してください。',
      minValue: '%{min} 以上の値にしてください。',
      maxValue: '%{max} より小さい値にしてください。',
      number: '数値で入力してください。',
      email: '正しいメールアドレスを入力してください。',
    },
  },
  walk30m: {
    notification: {
      execution_started: '開始しました。',
      aborted: '中止しました。',
      completed: '完了しました。',
      invitation_sent: '招待メールを送信しました。',
      invalid_invitation_token:
        '招待コードが不正です。管理者に招待メールの再送を依頼してください。',
      invalid_session_token:
        'セッション情報が不正です。ログインし直してください。',
      invalid_tenant_id:
        'セッションに関連づけられているテナント ID がリクエスト元と一致しません。',
    },
  },
};
