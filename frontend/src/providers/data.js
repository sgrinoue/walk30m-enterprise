import simpleRestProvider from 'ra-data-simple-rest';
import {httpClient} from './auth';

const dataProvider = simpleRestProvider(
  process.env.REACT_APP_API_URL,
  httpClient,
);

export default dataProvider;
