import {
  fetchUtils,
  AUTH_LOGOUT,
  AUTH_LOGIN,
  AUTH_CHECK,
  AUTH_GET_PERMISSIONS,
} from 'react-admin';
import jwt from 'jsonwebtoken';

const url = process.env.REACT_APP_API_URL;
const TOKEN = 'token';

function isExpired(token) {
  const decodedToken = jwt.decode(token);

  return !decodedToken || decodedToken.exp * 1000 < Date.now();
}

async function ensureOkResponse(response) {
  if (response.status < 200 || response.status >= 300) {
    const {message} = await response.json();

    localStorage.removeItem(TOKEN);

    throw new Error(message);
  }
}

async function refreshToken() {
  const token = localStorage.getItem(TOKEN);

  if (!isExpired(token)) return token;

  const response = await fetch(`${url}/session/refresh`, {
    method: 'GET',
    headers: {Authorization: `Bearer ${token}`},
  });

  await ensureOkResponse(response);

  const {token: refreshedToken} = await response.json();

  localStorage.setItem(TOKEN, refreshedToken);

  return refreshedToken;
}

async function attemptLogin(username, password) {
  const response = await fetch(`${url}/session`, {
    method: 'POST',
    body: JSON.stringify({username, password}),
    headers: {'Content-Type': 'application/json'},
  });

  await ensureOkResponse(response);

  const {token} = await response.json();

  localStorage.setItem(TOKEN, token);
}

export const httpClient = async (url, options = {}) => {
  const isCreatingAccount =
    url.split('/').pop() === 'accounts' && options.method === 'POST';

  if (!options.headers) {
    options.headers = new Headers({Accept: 'application/json'});
  }

  if (isCreatingAccount) {
    const response = await fetchUtils.fetchJson(url, options);

    if (response.status === 201) {
      const {
        json: {token},
      } = response;

      localStorage.setItem(TOKEN, token);
    }

    return response;
  } else {
    const token = await refreshToken();

    options.headers.set('Authorization', `Bearer ${token}`);

    return fetchUtils.fetchJson(url, options);
  }
};

export default async (type, params) => {
  const token = localStorage.getItem(TOKEN);

  switch (type) {
    case AUTH_LOGIN:
      const {username, password} = params;

      await attemptLogin(username, password);
      break;
    case AUTH_CHECK:
      if (!window.location.hash.match(/^#\/accounts\/create/) && !token) {
        throw new Error(
          'セッションの有効期限が切れています。ログインし直してください。',
        );
      }
      break;
    case AUTH_LOGOUT:
      localStorage.removeItem(TOKEN);
      break;
    case AUTH_GET_PERMISSIONS:
      if (!token) return null;

      const decodedToken = jwt.decode(token);

      return decodedToken.role;
    default:
      break;
  }
};
