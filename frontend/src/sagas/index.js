import {eventChannel} from 'redux-saga';
import {take, put, call, takeLatest, select} from 'redux-saga/effects';
import {LOCATION_CHANGE} from 'react-router-redux';
import {change, actionTypes as FormActions} from 'redux-form';
import {UPDATE, CRUD_CREATE_SUCCESS} from 'react-admin';
import saveAs from 'file-saver';
import getCalcService from '../services/calculation';
import getGeocoder from '../services/geocoder';
import {httpClient} from '../providers/auth';
import {
  CALCULATION_START,
  CALCULATION_PROGRESS,
  CALCULATION_COMPLETE,
  CALCULATION_ERROR,
  CALCULATION_SYNC,
  MAP_MARKER_MOVED,
  REQUEST_DOWNLOAD,
  ADDRESS_CHANGED,
  ADDRESS_CHOOSED,
  INVERSE_GEOCODE_DONE,
} from '../actions';

const baseUrl = process.env.REACT_APP_API_URL;

const syncAddressToCoordinate = function*(coordinate) {
  const service = yield call(getGeocoder);
  const result = yield call(
    () =>
      new Promise((resolve, reject) => {
        service.geocode({location: coordinate}, (result, status) => {
          resolve(status === 'OK' ? result[0].formatted_address : null);
        });
      }),
  );

  if (result) {
    yield put(change('record-form', 'originAddress', result));
    yield put(change('record-form', 'originCoordinate', coordinate));
  } else {
    yield put(change('record-form', 'originAddress', null));
  }
};

export const navigationSaga = function*() {
  yield takeLatest(LOCATION_CHANGE, function*({payload}) {
    window.scrollTo(0, 0);

    if (payload.pathname !== '/executions/create') return;

    const coordinate = yield select(
      state => state.explorer.initialValues.originCoordinate,
    );

    yield call(syncAddressToCoordinate, coordinate);
  });
};

export const executionSaga = function*() {
  yield takeLatest(CRUD_CREATE_SUCCESS, function*({
    meta: {resource},
    payload: {data},
  }) {
    if (resource !== 'executions') return;

    const service = yield call(getCalcService);

    service.start(
      new window.google.maps.LatLng(data.originCoordinate),
      data.time * 60,
      {
        ...data,
        anglePerStep: {
          SPEED: 25,
          BALANCE: 20,
          PRECISION: 15,
        }[data.preference],
      },
    );

    yield put({type: CALCULATION_START, payload: data.id});
  });
};

export const monitoringSaga = function*() {
  const channel = yield call(async () => {
    const service = await getCalcService();

    return eventChannel(emit => {
      const progressListener = (vertices, value) => {
        emit({type: CALCULATION_PROGRESS, payload: {vertices, value}});
      };
      const completeListener = vertices => {
        emit({type: CALCULATION_COMPLETE, payload: {vertices}});
      };
      const errorListener = error => {
        emit({type: CALCULATION_ERROR, payload: {error}});
      };

      service.addListener('progress', progressListener);
      service.addListener('complete', completeListener);
      service.addListener('error', errorListener);

      return () => {
        service.removeListener(progressListener);
        service.removeListener(completeListener);
        service.removeListener(errorListener);
      };
    });
  });

  while (true) {
    const event = yield take(channel);

    yield put(event);
  }
};

export const geocoderSaga = function*() {
  yield takeLatest(MAP_MARKER_MOVED, function*({payload}) {
    yield call(syncAddressToCoordinate, payload);
  });
};

export const registerAbortionSaga = function*() {
  yield takeLatest(CALCULATION_ERROR, function*({payload}) {
    const service = yield call(getCalcService);
    const id = yield select(state => state.execution.id);

    service.stop();

    yield put({
      type: CALCULATION_SYNC,
      payload: {
        id,
        data: {
          status: 'aborted',
        },
      },
      meta: {
        resource: 'executions',
        fetch: UPDATE,
        onSuccess: {
          notification: {
            body: 'walk30m.notification.aborted',
          },
        },
        onaFailure: {
          notification: {
            body: 'ra.notification.http_error',
            level: 'warning',
          },
        },
      },
    });
  });
};

export const registerCompletionSaga = function*() {
  yield takeLatest(CALCULATION_COMPLETE, function*({payload}) {
    const id = yield select(state => state.execution.id);

    yield put({
      type: CALCULATION_SYNC,
      payload: {
        id,
        data: {
          status: 'completed',
          resultPath: payload.vertices.map(vertex => vertex.toJSON()),
        },
      },
      meta: {
        resource: 'executions',
        fetch: UPDATE,
        onSuccess: {
          notification: {
            body: 'walk30m.notification.completed',
          },
        },
        onaFailure: {
          notification: {
            body: 'ra.notification.http_error',
            level: 'warning',
          },
        },
      },
    });
  });
};

export const handleDownloadSaga = function*() {
  yield takeLatest(REQUEST_DOWNLOAD, function*({payload: {id, format}}) {
    yield call(async () => {
      const response = await httpClient(
        `${baseUrl}/executions/${id}.${format}?download`,
        {
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
        },
      );

      saveAs(
        new Blob([response.body], {type: response.headers.get('Content-Type')}),
        response.headers.get('Content-Disposition').match(/filename=(.*)$/)[1],
      );
    });
  });
};

export const persistFormInputSaga = function*() {
  yield takeLatest(FormActions.CHANGE, ({meta: {form, field}, payload}) => {
    if (form !== 'record-form') return;

    const conditions = {
      ...JSON.parse(localStorage.getItem('saved_conditions') || '{}'),
      [field]: payload,
    };

    localStorage.setItem('saved_conditions', JSON.stringify(conditions));
  });
};

export const handleAddressChange = function*() {
  yield takeLatest(ADDRESS_CHANGED, function*({payload: address}) {
    yield new Promise(resolve => setTimeout(resolve, 500));

    if (address === '') return;

    const service = yield call(getGeocoder);
    const results = yield call(
      () =>
        new Promise(resolve =>
          service.geocode({address}, (result, status) => {
            resolve(status === 'OK' ? result : []);
          }),
        ),
    );

    yield put({type: INVERSE_GEOCODE_DONE, payload: results});
  });
};

export const handleAddressChoose = function*() {
  yield takeLatest(ADDRESS_CHOOSED, function*({
    payload: {
      formatted_address,
      geometry: {location},
    },
  }) {
    yield put(change('record-form', 'originAddress', formatted_address));
    yield put(change('record-form', 'originCoordinate', location.toJSON()));
  });
};
