async function getGeocoder() {
  if (window.google && window.google.maps && window.google.maps.Geocoder) {
    return new window.google.maps.Geocoder();
  }

  return new Promise(resolve => {
    window.requestAnimationFrame(async () => resolve(await getGeocoder()));
  });
}

export default getGeocoder;
