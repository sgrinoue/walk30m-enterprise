import Walk30m from '@walk30m/core';

let calcService = null;

async function getService() {
  if (calcService !== null) {
    return calcService;
  }

  if (window.google && window.google.maps) {
    calcService = new Walk30m();
    return calcService;
  }

  return new Promise(resolve => {
    window.requestAnimationFrame(async () => resolve(await getService()));
  });
}

export default getService;
