const express = require('express');
const logger = require('./logger');
const executions = require('./routes/executions');
const accounts = require('./routes/accounts');
const invitations = require('./routes/invitations');
const session = require('./routes/session');
const errorHandler = require('./concerns/errorHandler');
const cors = require('./concerns/cors');
const bodyParser = require('./concerns/bodyParser');
const withAuth = require('./concerns/auth');

const port = process.env.PORT || 8080;
const app = express();

app.use(cors);
app.use(bodyParser);
app.use(logger.requestLogger);

app.get('/', (req, res) => res.status(200).end());
app.use('/session', session);
app.use(
  '/accounts',
  (req, res, next) =>
    req.path === '/' && req.method === 'POST'
      ? next()
      : withAuth(req, res, next),
  accounts,
);
app.use('/executions', withAuth, executions);
app.use('/invitations', withAuth, invitations);

app.use(errorHandler);

app.listen(port, () => logger.info(`App is listening to ${port}`));
