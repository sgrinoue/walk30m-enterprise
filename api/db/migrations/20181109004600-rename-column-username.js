'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('Accounts', 'userName', 'id');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('Accounts', 'id', 'userName');
  },
};
