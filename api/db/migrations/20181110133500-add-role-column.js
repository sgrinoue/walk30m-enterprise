'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Accounts',
      'role',
      {
        type: Sequelize.ENUM('system', 'administrator', 'editor', 'viewer'),
        allowNull: false,
        defaultValue: 'viewer'
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Accounts', 'role');
    await queryInterface.sequelize.query('DROP TYPE "enum_Accounts_role";');
  },
};
