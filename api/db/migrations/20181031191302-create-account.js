'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Accounts',
      {
        tenantId: {
          primaryKey: true,
          type: Sequelize.STRING,
        },
        userName: {
          primaryKey: true,
          type: Sequelize.STRING,
        },
        displayName: {
          type: Sequelize.STRING,
        },
        password: {
          type: Sequelize.STRING,
        },
        lastSignIn: {
          type: Sequelize.DATE,
        },
      },
      {
        charset: 'utf8mb4',
        collate: 'utf8mb4_unicode_ci',
      },
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Accounts');
  },
};
