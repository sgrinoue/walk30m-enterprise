'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Counts', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING,
      },
      value: {
        type: Sequelize.INTEGER,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Counts');
  },
};
