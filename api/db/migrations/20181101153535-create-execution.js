'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Executions', {
      id: {
        primaryKey: true,
        allowNull: false,
        type: Sequelize.UUID,
      },
      tenantId: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      userId: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      startDateTime: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      originAddress: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      originCoordinate: {
        allowNull: false,
        type: Sequelize.GEOMETRY('POINT'),
      },
      mode: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      time: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      preference: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      avoidFerries: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      avoidTolls: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      avoidHighways: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      completeDateTime: {
        type: Sequelize.DATE,
      },
      resultPath: {
        type: Sequelize.GEOMETRY('POLYGON'),
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Executions');
  },
};
