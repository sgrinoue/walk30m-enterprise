const Sequelize = require('sequelize');
const URL = require('url-parse');

const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const hostname = process.env.DB_HOST;
const port = process.env.DB_PORT;
const db = process.env.DB_NAME;

const url = new URL(
  process.env.DATABASE_URL ||
    `mysql://${user}:${password}@${hostname}:${port}/${db}`,
);

module.exports = {
  username: url.username,
  password: url.password,
  database: url.pathname.slice(1),
  host: url.hostname,
  port: url.port,
  dialect: 'postgres',
  logging: false,
  operatorsAliases: Sequelize.Op,
};
