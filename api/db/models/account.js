module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define(
    'Account',
    {
      tenantId: {primaryKey: true, type: DataTypes.STRING},
      id: {primaryKey: true, type: DataTypes.STRING},
      displayName: DataTypes.STRING,
      password: DataTypes.STRING,
      lastSignIn: DataTypes.DATE,
      role: {type: DataTypes.STRING, allowNull: false, defaultValue: 'viewer'},
    },
    {
      timestamps: false,
    },
  );
  Account.associate = function(models) {
    // associations can be defined here
  };

  Account.prototype.toJSON = function() {
    const values = Object.assign({}, this.get());

    delete values.password;

    return values;
  };

  Account.prototype.signIn = async function() {
    await Account.update(
      {lastSignIn: new Date()},
      {
        where: {
          tenantId: this.tenantId,
          id: this.id,
        },
      },
    );
  };

  return Account;
};
