

const {omit} = require('lodash');
const shortUUID = require('short-uuid');

const uuidTranslator = shortUUID();

module.exports = (sequelize, DataTypes) => {
  const Execution = sequelize.define(
    'Execution',
    {
      id: {
        primaryKey: true,
        allowNull: false,
        type: DataTypes.UUID,
        get() {
          return uuidTranslator.fromUUID(this.getDataValue('id'));
        },
        set(val) {
          this.setDataValue('id', uuidTranslator.toUUID(val));
        },
      },
      tenantId: {allowNull: false, type: DataTypes.STRING},
      userId: {allowNull: false, type: DataTypes.STRING},
      startDateTime: {allowNull: false, type: DataTypes.DATE},
      originAddress: {allowNull: false, type: DataTypes.TEXT},
      originCoordinate: {
        allowNull: false,
        type: DataTypes.GEOMETRY('POINT'),
        get() {
          const {coordinates} = this.getDataValue('originCoordinate');

          return {lat: coordinates[1], lng: coordinates[0]};
        },
        set({lat, lng}) {
          this.setDataValue('originCoordinate', {
            type: 'POINT',
            coordinates: [lng, lat],
          });
        },
      },
      mode: {allowNull: false, type: DataTypes.STRING},
      time: {allowNull: false, type: DataTypes.INTEGER},
      preference: {allowNull: false, type: DataTypes.STRING},
      avoidFerries: {allowNull: false, type: DataTypes.BOOLEAN},
      avoidTolls: {allowNull: false, type: DataTypes.BOOLEAN},
      avoidHighways: {allowNull: false, type: DataTypes.BOOLEAN},
      status: {allowNull: false, type: DataTypes.STRING},
      completeDateTime: DataTypes.DATE,
      resultPath: {
        type: DataTypes.GEOMETRY('POLYGON'),
        get() {
          const {coordinates} = this.getDataValue('resultPath') || {};

          if (!coordinates) return null;

          return coordinates[0].map(coord => ({lat: coord[1], lng: coord[0]}));
        },
        set(coords) {
          if (!coords) {
            this.setDataValue('resultPath', null);
            return;
          }

          this.setDataValue('resultPath', {
            type: 'POLYGON',
            coordinates: [coords.map(({lat, lng}) => [lng, lat])],
          });
        },
      },
    },
    {
      timestamps: false,
    },
  );
  Execution.associate = function(models) {
    // associations can be defined here
  };

  Execution.prototype.toGeoJson = function() {
    return {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          id: `${this.id}:origin`,
          geometry: {
            type: 'Point',
            coordinates: [this.originCoordinate.lng, this.originCoordinate.lat],
          },
          properties: {
            address: this.originAddress,
          },
        },
      ].concat(
        this.status === 'completed'
          ? [
              {
                type: 'Feature',
                id: `${this.id}:result`,
                geometry: {
                  type: 'Polygon',
                  coordinates: [
                    this.resultPath.map(({lat, lng}) => [lng, lat]),
                  ],
                },
                properties: omit(
                  this.get(),
                  'id',
                  'originAddress',
                  'originCoordinate',
                  'resultPath',
                ),
              },
            ]
          : [],
      ),
    };
  };

  return Execution;
};
