const jwt = require('jsonwebtoken');
const {Account} = require('../db/models');
const {ClientError} = require('../errors');
const {JWT_ISSUER, JWT_TYPE_SESSION} = require('../constants');

const secret = process.env.SECRET;
const expiresIn = 60 * 5; // 5 minutes;

const issueToken = (userModel, overrides) => {
  const {tenantId: aud, id: sub, ...userData} = userModel.toJSON();

  return jwt.sign(
    {
      ...userData,
      iss: JWT_ISSUER,
      typ: JWT_TYPE_SESSION,
      aud,
      sub,
      exp: Math.round(new Date() / 1000) + expiresIn,
      ...overrides,
    },
    secret,
  );
};

const updateToken = async token =>
  new Promise((resolve, reject) => {
    jwt.verify(
      token,
      secret,
      {ignoreExpiration: true},
      async (err, {aud: tenantId, sub: id, iat, role} = {}) => {
        if (err)
          return reject(
            new ClientError(
              'セッション情報が無効です。ログインし直してください。',
              err,
            ),
          );

        try {
          const user = await Account.findOne({where: {tenantId, id}});

          if (!user || user.role !== role)
            return reject(
              new ClientError(
                'アカウント情報が変更されたため, ログインし直してください。',
              ),
            );

          const updatedToken = issueToken(user, {
            iat, // don't update issue timestamp
          });

          return resolve(updatedToken);
        } catch (e) {
          return reject(e);
        }
      },
    );
  });

module.exports = {
  issueToken,
  updateToken,
  secret,
};
