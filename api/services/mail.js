const axios = require('axios');

function sendMail(fromMailAddress, fromName, targets, subject, body) {
  return axios.post(
    'https://api.sendgrid.com/v3/mail/send',
    {
      personalizations: targets.map(({email, substitutions}) => ({
        to: [{email}],
        substitutions,
        subject,
      })),
      from: {name: fromName, email: fromMailAddress},
      content: [{type: 'text/plain', value: body}],
    },
    {
      headers: {
        Authorization: `Bearer ${process.env.SEND_GRID_API_KEY}`,
      },
    },
  );
}

module.exports = sendMail;
