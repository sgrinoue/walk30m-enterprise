const jwt = require('jsonwebtoken');
const {ClientError} = require('../errors');
const {JWT_ISSUER, JWT_TYPE_INVITATION} = require('../constants');

const expiresIn = 60 * 60 * 72;
const secret = process.env.SECRET;

const verifyToken = token => {
  const {sub: id, aud: tenantId, typ, role} = jwt.verify(token, secret);

  if (typ !== JWT_TYPE_INVITATION) {
    throw new ClientError('walk30m.notification.invalid_invitation_token');
  }

  return {id, tenantId, role};
};

const issueToken = (tenantId, id, role) =>
  jwt.sign(
    {
      iss: JWT_ISSUER,
      typ: JWT_TYPE_INVITATION,
      aud: tenantId,
      sub: id,
      exp: Math.round(new Date() / 1000) + expiresIn,
      role,
    },
    secret,
  );

module.exports = {
  issueToken,
  verifyToken,
};
