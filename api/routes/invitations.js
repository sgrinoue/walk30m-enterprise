const express = require('express');
const {issueToken} = require('../services/invitation');
const sendMail = require('../services/mail');
const {Account, Sequelize} = require('../db/models');
const {ClientError} = require('../errors');

const app = express();
const invitationMailBody = `walk30m for Business へようこそ。
72時間以内に以下のリンクをクリックしてアカウントの登録を完了してください。

%registerUrl%

なお、メールに心当たりがない場合はリンクをクリックせず、メールを破棄してください。
`;

async function alreadyExistingAccounts(tenantId, ids) {
  const results = Account.findAll({
    where: {
      tenantId,
      id: {
        [Sequelize.Op.in]: ids,
      },
    },
    attributes: ['id'],
  });

  return results.map(({id}) => id);
}

app.post('/', async (req, res, next) => {
  try {
    const {displayName: invitorName, tenantId} = req.user;
    const subject = `${invitorName}さんがあなたを ${tenantId} に招待しています。`;
    const {targets} = req.body;
    const existingAccounts = await alreadyExistingAccounts(
      tenantId,
      targets.map(({email}) => email),
    );

    if (existingAccounts.length > 0) {
      const accountsExpr = existingAccounts.join(',');

      throw new ClientError(
        `以下のメールアドレスはすでに登録済みです: ${accountsExpr}`,
      );
    }

    await sendMail(
      'noreply@walk30m.com',
      'walk30m for Business',
      targets.map(({email, role}) => {
        const token = issueToken(tenantId, email, role);

        return {
          email,
          substitutions: {
            '%registerUrl%': `${tenantId}/#/accounts/create?token=${token}`,
          },
        };
      }),
      subject,
      invitationMailBody,
    );
    res.json({});
  } catch (e) {
    next(e);
  }
});

module.exports = app;
