const express = require('express');
const bcrypt = require('bcrypt');
const {mapValues} = require('lodash');
const {Account, Sequelize} = require('../db/models');
const {issueToken} = require('../services/session');
const {verifyToken} = require('../services/invitation');

const app = express();

async function getHashedPassword(password) {
  return new Promise((resolve, reject) =>
    bcrypt.hash(password, 10, (err, hash) =>
      err ? reject(err) : resolve(hash),
    ),
  );
}

app.post('/', async (req, res, next) => {
  try {
    const {token, displayName, password, passwordConfirm} = req.body;

    if (password !== passwordConfirm) {
      return res.status(400).json({
        message: '確認用パスワードが一致しません。',
      });
    }

    const {tenantId, id, role} = verifyToken(token);
    const existingAccount = await Account.findOne({where: {tenantId, id}});

    if (existingAccount) {
      return res.status(400).json({
        message: '登録済みのログイン ID です。',
      });
    }

    const user = await Account.create({
      tenantId,
      id,
      displayName,
      password: await getHashedPassword(password),
      role,
    });

    await user.signIn();

    return res.status(201).json({
      user: user.toJSON(),
      token: issueToken(user),
    });
  } catch (e) {
    return next(e);
  }
});

app.get('/', async (req, res, next) => {
  try {
    const {filter, range = [], sort} =
      mapValues(req.query || {}, value => JSON.parse(value)) || {};
    const offset = range[0] || 0;
    const limit = range[1] - offset || 25;

    const {count, rows: accounts} = await Account.findAndCountAll({
      where: {
        tenantId: req.user.tenantId,
        ...filter,
      },
      order: [sort || ['lastSignIn', 'DESC']],
      limit,
      offset,
    });
    const lastIndex = Math.min(offset + limit, count);

    return res
      .status(200)
      .set('Content-Range', `accounts ${offset}-${lastIndex}/${count}`)
      .json(accounts.map(account => account.toJSON()));
  } catch (e) {
    return next(e);
  }
});

app.get('/:id', async (req, res, next) => {
  try {
    const account = await Account.findOne({
      where: {
        tenantId: req.user.tenantId,
        id: req.params.id,
      },
    });

    if (!account) return res.status(404).end();

    return res.status(200).json(account);
  } catch (e) {
    return next(e);
  }
});

app.put('/:id', async (req, res, next) => {
  try {
    const {password, passwordConfirm, displayName} = req.body;

    if (req.params.id !== req.user.id) {
      return res.status(401).json({
        message: '操作の権限がありません。',
      });
    }

    if (password !== undefined && password !== passwordConfirm) {
      return res.status(400).json({
        message: '確認用パスワードが一致しません。',
      });
    }

    const [, [updatedAccount]] = await Account.update(
      {
        displayName,
        ...(password === undefined
          ? {}
          : {password: await getHashedPassword(password)}),
      },
      {
        where: {
          tenantId: req.user.tenantId,
          id: req.params.id,
        },
        returning: true,
      },
    );

    return res.json(updatedAccount.toJSON());
  } catch (e) {
    return next(e);
  }
});

app.delete('/:id', async (req, res, next) => {
  try {
    if (req.params.id === req.user.id) {
      return res.status(401).json({
        message: '自分自身のアカウントを削除することはできません。',
      });
    }

    const affectedCount = await Account.destroy({
      where: {
        tenantId: req.user.tenantId,
        id: req.params.id,
        role: {
          [Sequelize.Op.ne]: 'system',
        },
      },
    });

    if (affectedCount === 0) return res.status(404).end();
    return res.status(204).end();
  } catch (e) {
    return next(e);
  }
});

module.exports = app;
