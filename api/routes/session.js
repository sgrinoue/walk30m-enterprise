const express = require('express');
const passport = require('passport');
const {issueToken, updateToken} = require('../services/session');

const app = express();

app.post('/', (req, res, next) => {
  passport.authenticate('local', {session: false}, async (err, user) => {
    try {
      if (err || !user) {
        return res.status(401).json({
          message: err || 'エラーが発生しました。',
        });
      }

      await user.signIn();

      return res.json({user: user.toJSON(), token: issueToken(user)});
    } catch (e) {
      return next(e);
    }
  })(req, res);
});

app.get('/refresh', async (req, res, next) => {
  try {
    const oldToken = (req.get('Authorization') || '').replace(/^Bearer /, '');

    return res.json({token: await updateToken(oldToken)});
  } catch (e) {
    return next(e);
  }
});

module.exports = app;
