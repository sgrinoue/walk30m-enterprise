const express = require('express');
const shortUUID = require('short-uuid');
const {mapValues} = require('lodash');
const formatXml = require('xml-formatter');
const tokml = require('tokml');
const {Op} = require('sequelize');
const {Execution} = require('../db/models');
const {ClientError} = require('../errors');

const app = express();
const uuidTranslator = shortUUID();

app.post('/', async (req, res, next) => {
  try {
    const record = {
      ...req.body,
      id: uuidTranslator.new(),
      tenantId: req.user.tenantId,
      userId: req.user.id,
      startDateTime: new Date(),
      status: 'started',
    };

    await Execution.create(record);

    res.status(201).json(record);
  } catch (e) {
    next(e);
  }
});

app.get('/', async (req, res, next) => {
  try {
    const {filter = {}, range = [], sort} =
      mapValues(req.query || {}, value => JSON.parse(value)) || {};
    const offset = range[0] || 0;
    const limit = range[1] - offset || 25;
    const {count, rows: records} = await Execution.findAndCountAll({
      where: {
        tenantId: req.user.tenantId,
        ...(filter.status ? {status: {[Op.in]: filter.status}} : {}),
        ...(filter.mode ? {mode: {[Op.in]: filter.mode}} : {}),
        ...(filter.originAddress
          ? {originAddress: {[Op.like]: `%${filter.originAddress}%`}}
          : {}),
        ...(filter.userId ? {userId: {[Op.in]: filter.userId}} : {}),
      },
      offset,
      limit,
      order: [sort || ['startDateTime', 'DESC']],
    });
    const lastIndex = Math.min(offset + limit, count);

    res
      .status(200)
      .set('Content-Range', `executions ${offset}-${lastIndex}/${count}`)
      .json(records);
  } catch (e) {
    next(e);
  }
});

app.put('/:id', async (req, res, next) => {
  try {
    const record = await Execution.findOne({
      where: {
        id: uuidTranslator.toUUID(req.params.id),
        tenantId: req.user.tenantId,
        status: 'started',
      },
    });

    if (!record) return res.status(404).end();

    switch (req.body.status) {
      case 'completed':
        return res.status(200).json(
          await record.update({
            completeDateTime: new Date(),
            resultPath: req.body.resultPath,
            status: 'completed',
          }),
        );
      case 'aborted':
        return res.status(200).json(
          await record.update({
            status: 'aborted',
          }),
        );
      default:
        throw new ClientError(`Unknown status ${req.body.status}`);
    }
  } catch (e) {
    return next(e);
  }
});

app.get('/:id', async (req, res, next) => {
  try {
    const [id, extension] = req.params.id.split('.');
    const record = await Execution.findOne({
      where: {
        id: uuidTranslator.toUUID(id),
        tenantId: req.user.tenantId,
      },
    });

    if (!record) return res.status(404).end();

    if (req.query.download !== undefined) {
      res.set('Content-Disposition', `attachment; filename=${req.params.id}`);
    }

    switch (extension) {
      case 'geojson':
        return res
          .set('Content-Type', 'application/vnd.geo+json')
          .send(JSON.stringify(record.toGeoJson(), null, '  '));
      case 'kml':
        return res
          .set('Content-Type', 'application/vnd.google-earth.kml+xml')
          .send(formatXml(tokml(record.toGeoJson())));
      default:
        return res.json(record);
    }
  } catch (e) {
    return next(e);
  }
});

module.exports = app;
