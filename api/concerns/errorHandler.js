const logger = require('../logger');

// eslint-disable-next-line no-unused-vars
module.exports = (error, req, res, next) => {
  logger.error(
    `${error.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`,
    {user: req.user, stack: error.stack},
  );

  switch (error.name) {
    case 'ClientError':
      return res.status(400).json({message: error.message});
    case 'SequelizeValidationError':
      return res.status(400).json({message: error.message});
    default:
      return res.status(500).json({
        message:
          '予期しないエラーが発生しました。時間をおいて再度お試しください。',
      });
  }
};
