const cors = require('cors');

module.exports = cors({
  allowedHeaders: ['Content-Type', 'Authorization'],
  exposedHeaders: ['Content-Range', 'Content-Disposition'],
});
