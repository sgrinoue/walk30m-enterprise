const passportJWT = require('passport-jwt');
const {secret} = require('../../services/session');
const {JWT_ISSUER, JWT_TYPE_SESSION} = require('../../constants');

module.exports = new passportJWT.Strategy(
  {
    jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret,
    passReqToCallback: true,
    issuer: JWT_ISSUER,
  },
  (req, {aud, typ, sub, ...jwtPayload}, cb) => {
    if (req.get('Origin') !== aud) {
      return cb('walk30m.notification.invalid_tenant_id', false);
    }

    if (typ !== JWT_TYPE_SESSION) {
      return cb('walk30m.notification.invalid_session_token', false);
    }

    return cb(null, {tenantId: aud, id: sub, ...jwtPayload});
  },
);
