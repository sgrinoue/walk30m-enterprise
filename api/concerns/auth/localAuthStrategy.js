const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const {Account} = require('../../db/models');

module.exports = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true,
  },
  async (req, username, password, cb) => {
    const user = await Account.findOne({
      where: {
        tenantId: req.get('Origin'),
        id: username,
      },
    });

    if (user) {
      const isValid = await bcrypt.compare(password, user.password);

      if (isValid) return cb(null, user);
    }

    return cb('パスワードが間違っているか、存在しないユーザーです。', null);
  },
);
