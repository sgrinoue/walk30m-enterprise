const passport = require('passport');
const localAuthStrategy = require('./localAuthStrategy');
const jwtSessionStrategy = require('./jwtSessionStrategy');

passport.use(localAuthStrategy);
passport.use(jwtSessionStrategy);

module.exports = passport.authenticate('jwt', {session: false});
