module.exports = {
  JWT_ISSUER: 'walk30m',
  JWT_TYPE_SESSION: 'session',
  JWT_TYPE_INVITATION: 'invitation',
};
