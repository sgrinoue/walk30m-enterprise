const winston = require('winston');
const chalk = require('chalk');
const {chain, pick} = require('lodash');
const expressWinston = require('express-winston');
const {LoggingWinston} = require('@google-cloud/logging-winston');

const credentials = JSON.parse(process.env.GCP_SERVICE_ACCOUNT_KEY);

const consoleFormat = addDate =>
  winston.format.printf(info => {
    const {level, message, stack} = info;
    const datePart = addDate ? `${chalk.dim(new Date().toISOString())} ` : '';

    if (stack)
      return `${datePart}${level} ${message}

==== STACKTRACE ====

${stack}
`;

    return `${datePart}${level} ${message}`;
  });

const stackdriverSink = new LoggingWinston({
  projectId: credentials.project_id,
  credentials,
  format: winston.format.combine(winston.format.json()),
});

const consoleSink = addDate =>
  new winston.transports.Console({
    format: winston.format.combine(
      winston.format.colorize(),
      consoleFormat(addDate),
    ),
  });

const logger = winston.createLogger({
  exitOnError: false,
  transports:
    process.env.NODE_ENV === 'production'
      ? [consoleSink(false), stackdriverSink]
      : [consoleSink(true)],
});

const sensitiveDataReductionFilter = (req, propName) => {
  if (propName === 'headers') {
    return chain(req)
      .get(propName)
      .cloneDeep()
      .assign(
        pick(
          {
            authorization: '[REDACTED]',
            cookie: '[REDACTED]',
          },
          Object.keys(req[propName]),
        ),
      )
      .value();
  }
  return req[propName];
};

logger.requestLogger = expressWinston.logger({
  exitOnError: false,
  transports:
    process.env.NODE_ENV === 'production'
      ? [stackdriverSink]
      : [consoleSink(true)],
  meta: true,
  statusLevels: true,
  requestFilter: sensitiveDataReductionFilter,
  msg: [
    '{{req.method}}',
    '{{req.url}}',
    '{{res.statusCode}}',
    '{{res.responseTime}}ms',
    '{{req.ip}}',
    '{{(req.user || {}).tenantId}}',
    '{{(req.user || {}).id}}',
  ].join(' '),
  colorize: false,
});

module.exports = logger;
